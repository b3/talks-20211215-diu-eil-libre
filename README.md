On trouve ici les sources Markdown (format Pandoc) et une version PDF
des présentations que j'ai faites le 15 décembre 2021 et le 29 juin
2022 dans le cadre du [Diplôme Inter-Universitaire *Enseigner
l'informatique au
lycée*](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md)
de l'Université de Lille.

Il s'agissait de présenter deux thématiques proches à un public
d'enseignants de lycée pas encore informaticiens (mais bien
sympathisants ;-) venus se préparer à enseigner la spécialité
*Numérique et Sciences Informatiques* :

- dans un premier temps expliquer les licences et les logiciels libres
- dans un second temps essayer de faire une introduction/sensibilisation au
  ressources éducatives libres et au partage.

J'espère ne pas y avoir raconté trop de sottises.
