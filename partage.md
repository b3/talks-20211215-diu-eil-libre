---
#
# Pour l'instant construit pour du pdf (LaTeX/Beamer) seulement avec toutes les extensions pandoc.
# 
# À compiler par : pandoc -t beamer --pdf-engine-opt=-shell-escape --template=etc/pandoc-beamer.tex --lua-filter=etc/pandoc-latex.lua --slide-level=2 --wrap=preserve --standalone -V aspectratio=169 partage.md -o partage.pdf
#
title: Informatique et partage
subtitle: Université de Lille - DIU EIL
author:
- Bruno BEAUFILS
date: 29 juin 2022
titlebackground: rel-fr
titlegraphic:
- file: logo-univ-lille
  width: .2
  nl: true
- file: logo-diu-eil
  width: .1
  nl: true
- file: logo-cc-by-nc-sa
  width: .1
keywords: diu-eil, informatique rel, oer,
license: ccByNCSA # pas utilisé pour l'instant
theme: ulille
fontsize: 10pt
classoption: t
---

Introduction
============

## Résumé de l'épisode précédent {.allowframebreaks}

### Droits

- *propriété intellectuelle*
    - **littéraire et artistique**
    - industrielle
- droits
    - moral : paternité, divulgation, integrité, repentir
    - patrimonial : représentation, reproduction
- [exceptions aux droits patrimoniaux](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006278917)
    - plusieurs types d'exception (autorisation d'utilisation)
    - exception *pédagogique*
        - accords sectoriels
        - logiciel exclus de l'exception pédagogique

### Logiciels libres

- contrat entre éditeur et utilisateur = licence

- licence fixe ce que l'*ayant-droit* permet sur son logiciel

- licence libre autorise
    - **utilisation sans restrictions**
      \dotfill
      liberté 0
    - **étude** et **modification**
      \dotfill
      liberté 1
    - **redistribution**
      \dotfill
      liberté 2
    - **distribution des versions modifiées**
      \dotfill
      liberté 3

### Creative Commons

- Licences modulables sous 4 axes
    - paternité
    - utilisation commerciale
    - modification
    - conditions de partage
- 6+1 licences différentes, avec des signalétiques adaptées, applicables à toutes les oeuvres de l'esprit

| CC-0    | CC-BY | CC-BY-SA | CC-BY-ND | CC-NY-NC  | **CC-BY-NC-SA** | CC-BY-NC-ND |
|:-------:|:-----:|:--------:|:--------:|:---------:|:---------------:|:-----------:|
| \cczero | \ccby | \ccbysa  | \ccbynd  | \ccbynceu | \ccbyncsaeu     | \ccbyncndeu |


## Contribuer aux logiciels libres

::: { .center }
Utiliser c'est déjà participer mais **contribuer** c'est mieux !
:::

\pause

Il ne faut pas se retenir : c'est simple, facile et gratifiant

- **commencer doucement**
  - remontée de problèmes, bugs
  - documentation
    - traduction
    - relecture/correction
    - rédaction

\pause

- **commencer sûrement**
  - s'informer [Framablog](https://www.framablog.org)
  - soutenir (adhérer, **participer** ou financer) les associations
    - [APRIL](https://www.april.org)
    - **[Framasoft](https://framasoft.org)**
    - [LQDN](https://www.laquadrature.net/fr)
    - [FFDN](https://www.ffdn.org)
  - *évangéliser* autour de soi

\pause


::: { .center }
**Comment utiliser tout ça dans votre quotidien d'enseignants en informatique ?**
:::

Outils
======

## Les outils que (maintenant) vous connaissez

Vous avez vu les outils essentiels

- **description de document**
  \dotfill\ 
  [Markdown](https://commonmark.org) 
    - texte
    - simple
    - transformable en d'autres format faciement (via [pandoc](https://pandoc.org) par exemple)
        - dites adieu à Word !
- **gestion de version**
    \dotfill\ 
    [git](https://git-scm.com)
    - serveur de partage
      \hfill\ 
      [*gitlab*](https://about.gitlab.com)
- **langage de programmation**
  \dotfill\ 
  [Python](https://python.org)
- **système d'exploitation**
  \dotfill\ 
  [GNU](https://gnu.org)/[Linux](https://fr.wikipedia.org/wiki/Linux)

Ils suffisent à **tout faire** \pause ou presque :-)

\pause\vfill

C'est *en gros* ce que j'utilise pour mes cours

## Ceux qui vous manquent

- **gestion de figures / images**
    - privilégier les formats vectoriels (**SVG**)
    - outils : [Inkscape](https://inkscape.org/fr) / [LibreOffice Draw](https://fr.libreoffice.org/discover/draw) / [DrawIO](https://www.diagrams.net) ([en ligne](https://draw.io))
    - séparer figures et document
    
    \pause

- **gestion de présentation et déroulé pédagogique**
    - [Jupyter](https://jupyter.org) (et ses Notebook)
    - utilise markdown et python
    - simplifie la prise en main et l'exécution des exercices

\pause

::: { .center }
**Des formats de fichier simples garantissent un partage facile**
:::


## Des services en ligne : la galaxie *Framasoft* { .allowframebreaks }

:::::: {.columns}
::: {.column width=25% }
[![](img/degooglisons-internet.png){ width=80% }](https://degooglisons-internet.org/fr/list)

[![](img/logo-chatons-org.svg){ width=10% center }](https://www.chatons.org)\ 
:::
::: {.column width=72% }
- beaucoup de services différents (alternative aux GAFAM)
    - [Framagit](https://framagit.org)
    - [Framapad](https://framapad.org)
    - [Framatube](https://framatube.org)
    - ...
- hébergés
    - sur les serveurs de l'association : [Dégooglisons Internet](https://degooglisons-internet.org/fr/list)
    - sur les serveurs de quelqu'un de confiance
        - *Collectif des Hébergeurs Alternatifs,Transparents, Ouverts, Neutres et Solidaires*
        - **<https://alt.framasoft.org/fr/>**
    - sur votre propre *serveur* (lycée, associations, personnels, etc.)
        - [Le jardin de Framacloud](https://framacloud.org/fr/cultiver-son-jardin/) contient les recettes d'installation

    - sur les serveurs du ministère **<https://apps.education.fr>**
        - services orientés enseignement à distance
        - une instance par académie
:::
::::::


## Freeduc-JBART { .allowframebreaks }

### « Mon lycée ne me permet pas d'utiliser Linux ! »

- Ça va **forcément** venir (dans le [programme](http://cache.media.education.gouv.fr/file/CSP/41/2/1e_Numerique_et_sciences_informatiques_Specialite_Voie_G_1025412.pdf) il faut utiliser un **système d'exploitation libre**)
    - conditions pas idéales pour l'instant dans l'académie
    - il faut un peu de patience 
- On peut s'arranger sans *trop* se fatiguer
    - grâce à [Georges KHAZNADAR](http://georges.khaznadar.fr/docs/speeches)
        - enseignant au lycée Jean BART de Dunkerque
        - développeur Debian
        - créateur/mainteneur d'une distribution Linux *live* pour le lycée
    - via [Freeduc-JBART](https://usb.freeduc.org/jbart.html) 
        1. télécharger puis décompresser le fichier [`jbart-19.06-16G.iso.gz`](https://usb.freeduc.org/freeduc-usb/freeduc-jbart/19.06/jbart-19.06-16G.iso.gz)
        2. installer le logiciel [Etcher](https://www.balena.io/etcher/) (*disponible sous Linux/Mac/Windows*)
        3. graver le fichier sur une clé de 16 Go
        4. démarrer un ordinateur à partir de la clé gravée

Détails sur <https://usb.freeduc.org/freeduc-usb/freeduc-jbart/index.fr.html>

**Même environnement au lycée qu'à la maison**


## { .plain }

| Le bureau de Freeduc-JBART                      |
|:-----------------------------------------------:|
| ![](img/freeduc-jbart-desktop.png){ width=60% } |


## Outils vs Ressources

Beaucoup d'outils mais le problème principal reste ...

\pause

::: { .center }
**les ressources pégagogiques**
:::

\pause

Elles doivent être

- disponibles
- adéquates
- sûres
- adaptables

\pause

Construire des ressources pédagogiques nécessite 

- des ressources (images, figures, vidéos, etc.)
- des idées
- du temps


Ressources pédagogiques
=======================

## Droits et utilisation

Construire des ressources pédagogiques nécessite des images, figures, vidéos, etc.

- Attention l'exception *pédagogique* peut-être **ambigue**
    - accords sectoriels
    - **toujours** vérifier la licence des ressources utilisées
    - éviter d'utiliser des ressources (images, vidéos, etc.) sans connaître la licence d'usage
        - ne pas hésiter à **contacter l'auteur** si nécessaire

- Une solution : **n'utiliser que des ressources *sûres***
    - avec par exemple une licence [CC](http://creativecommons.fr)

- Quelques services sûrs
    - Médiathèque : [Wikimedia Commons](https://fr.wikipedia.org/wiki/Wikimedia_Commons)
        - les ressources de Wikipedia sont issus de la [médiathèque Commons](http://commons.wikimedia.org)
    - Moteur de recherche 
        - ad-hoc comme [CC Search](https://search.creativecommons.org)
        - option des moteurs de recherche *classiques* (souvent un peu [dur à trouver](https://www.skell.fr/prof/Fic15licencesCC.pdf))
        <!-- tout en bas de https://www.google.com/advanced_image_search -->

\pause

::: { .center }
Et pour les cours et exercices ?
:::

## Ressources Éducatives Libres (*Open Educational Resources*)

> Les ressources éducatives libres (REL) sont des matériaux d'enseignement,
> d'apprentissage ou de recherche appartenant au **domaine public** ou **publiés
> avec une licence de propriété intellectuelle permettant leur utilisation,
> adaptation et distribution** à titre gratuit.

\pause

- Définition par l'[UNESCO](https://fr.unesco.org/themes/tic-education/rel) en 2002
    - Effort également supporté par l'OCDE, le Commonwealth et de nombreux autres organismes internationaux
    - [Recommandations de l'UNESCO aux États](https://en.unesco.org/oer/paris-declaration) pour favoriser et encourager les REL en 2012
    - Quelques tensions sur la définition concernant leur nature, source et leur degré d'*ouverture*

\pause

- Objectifs
    - **création et distribution de ressources éducatives libres et gratuites**
    - **ressources d'apprentissage *et* de soutien pour les enseignants**

\pause

- Beaucoup de projets de développement un peu partout et à tous les niveaux
    - [Open Education Consortium](https://www.oeconsortium.org)
    - [Wikieducator](https://wikieducator.org)

\pause
    
- Il reste du travail de **structuration des communautés** et de **dissémination**
    - surtout en informatique (paradoxalement)
        - peu de pays ont pris conscience *tôt* du rôle de l'enseignement de l'informatique
        - *contre-exemple* britanique

<!-- https://www.capetowndeclaration.org/read-the-declaration -->
<!-- https://www.capetowndeclaration.org -->


## La liberté pour les contenus éducatifs 

Les REL sont des oeuvres du domaine publique ou distribués de manière à
garantir à l'utilisateur une permission gratuite et permanente de s'engager
dans les [**activités 5R**](http://opencontent.org/definition)

\pause

- **RETENIR**

  obtenir, dupliquer, conserver, le contenu autant de fois ou aussi longtemps que vous le voulez

\pause

- **RÉUTILISER**

  utiliser le contenu de diverses manières selon vos propres fins

\pause

- **RÉVISER**

  adapter, modifier ou traduire la ressource

\pause

- **REMIXER**

  combiner la ressource (ou ses modifications) à une autre

\pause

- **REDISTRIBUER**

  partager la ressource (ou ses modifications) avec d’autres 

\pause

::: { .center }
**Proches de l'idée du logiciel libre**
::::

## La liberté pour les contenus éducatifs (originale)

- **Retain** - the right to make, own, and control copies of the content
    - (e.g., download, duplicate, store, and manage)
    \pause
- **Reuse** - the right to use the content in a wide range of ways
    - (e.g., in a class, in a study group, on a website, in a video)
    \pause
- **Revise** - the right to adapt, adjust, modify, or alter the content itself
    - (e.g., translate the content into another language)
    \pause
- **Remix** - **the right to combine the original or revised content with other material to create something new**
    - (e.g., incorporate the content into a mashup)
    \pause
- **Redistribute** - the right to share copies of the original content, your revisions, or your remixes with others
    - (e.g., give a copy of the content to a friend)

## Contribuer à la production

- Le mouvement du logiciel libre a réussi grâce aux **communautés** de développeurs et d'utilisateurs
    - la mise en **commun** a été le point crucial
- Le mouvement des REL réussira s'il réussi à créer des communautés aussi dynamiques
    - vous êtes le public rêvé pour ça
    - vous avez les outils et le savoir-faire pour créer des ressources libres
    - vous avez **intérêt** à contribuer pour faciliter votre travail
    
\pause

Quelques *conseils*
    
- [Privilégier la licence Creative Commons Paternité (CC BY) dans l’éducation](https://framablog.org/2009/12/02/licence-creative-commons-paternite-et-education/)
- Utiliser les formats simples
    - facilite le mélange (*remix*)
- Demander de l'aide quand vous en avez besoin
    - à nous
    - entre vous


Des idées et du temps
=====================

## Volonté politique

::: { .center }
*« La route est longue mais la voie est libre »* (Framasoft)

\pause

*« C'est lent mais ça commence à bouger »*

\pause
:::

- **DNE** : [Direction du Numérique pour l'Éducation](https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983)
    - chef de projet **logiciels et ressources éducatives libres** et **mixité dans les filières du numérique**
    - [Alexis KAUFFMAN](https://fr.wikipedia.org/wiki/Alexis_Kauffmann) (alias [framaka](https://twitter.com/framaka) : cofondateur de Framasoft)

      <!--![](img/framaka.jpg){ width=10% .center }-->
      
    - **Un allié dans la place**

\pause

- **DANE** : Délégation Académique au Numérique Éducatif
    - certaines académies sont **très** actives (ordre alphabétique ;-)
        - [Dijon](https://dane.ac-dijon.fr) (via [EOLE](https://pcll.ac-dijon.fr/eole) par exemple)
        - [Lyon](https://dane.ac-lyon.fr/spip/)
        - [Versailles](https://www.dane.ac-versailles.fr/)

## Construire une communauté informatique large

Favoriser les rencontres entre 

- enseignants-chercheurs en informatique
- *enseignants-chercheurs en science de l'éducation*
- **praticiens** (aka les gens qui font le travail pour de vrai : **vous**)

::: { .center }
**Moments d'échanges important pour forger une communauté**
:::

\pause

Quelques exemples accessibles

- [**Journée de l'Enseignement de l'Informatique et de l'Algorithmique**](http://jeia.fil.univ-lille1.fr) 
  \hfill\ 
  [`@JeiaLille`](https://twitter.com/JeiaLille)
  - tous les ans (*hors COVID*) ici (à l'Université de Lille)
- [**Journée du Libre Éducatif**](https://dane.ac-lyon.fr/spip/Journee-Du-Libre-Educatif-1er)
  - début cette année à Lyon le 1er avril 2022
  - aura lieu l'année prochaine sans doute dans une autre académie
- [**Journées enseignement de la SIF**](https://www.societe-informatique-de-france.fr/les-journees-sif)
    - une à deux fois par an pour fédérer la communauté autour de thèmes 

\pause

- [**educode.be**](http://www.educode.be)
  - colloque annuel international dédié à l’éducation, aux pratiques et la recherche dans le *numérique*
- [**Didapro - Didastic**](https://www.didapro.org)
  - colloque francophone de didactique de l'informatique

## Promouvoir SNT et NSI

Quelques actions nationales commencent à voir le jour

- [**Les Trophées NSI**](https://trophees-nsi.fr)
    - concours ouverts aux élèves de première et de terminale
    - organisés par territoire
- [**Journée Nationale NSI**](https://journee-nsi.fr)
    - une journée de promotion des spécialités informatique au lycée
    - protéiforme
    - évènements à organiser avec
        - Université
        - Branche et association professionnelle
- **Les 4 piliers de l'informatique**
    - 4 vidéos grand public de 10 minutes
    - production de la [Société Informatique de France](https://www.societe-informatique-de-france.fr) avec [L'esprit sorcier](https://espritsorcier.org)
- Concours [**Octet Vidéo**](https://www.societe-informatique-de-france.fr/mediation/concours-video-de-la-sif-octet-video/)
    - *L'informatique en 256 secondes*
    - Un concours vidéo annuel pour transmettre la passion de l'informatique

::: { .center }

Tout ça est perfectible (**manque de relais**)

:::

## Échanger entre collègues : forum NSI de l'AEIF

::: { .center }
[![](img/forum-nsi.png){ width=70% }](https://mooc-forums.inria.fr/moocnsi)

**<https://mooc-forums.inria.fr/moocnsi>**
:::


## Références { .allowframebreaks }

### Échanger

- Salon Tchap : [Logiciels et Ressources Libres dans l'éducation](https://tchap.gouv.fr/#/room/#LibreducationxZNclO9fwZr:agent.education.tchap.gouv.fr)
- [Forum NSI](https://mooc-forums.inria.fr/moocnsi)
- Associations
    - [**AEIF**](https://aeif.fr)
    - [SIF](https://www.societe-informatique-de-france.fr)
    - **adhérez !** *(la taille est importante pour être entendu)*

### Réutiliser

- [Les décodeuses du numérique](https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique)
  
   une BD et des contenus prêt à l'emploi 
- [Cours SNT, NSI](https://pixees.fr/informatiquelycee/)
   
   un cours complet de [David ROCHE](https://twitter.com/davR74130) sous licence `CC0`
- [InfoSansOrdi](https://github.com/InfoSansOrdi)

   des ressources pour faire de l'informatique sans électricité (`m99` par exemple)

\pagebreak

### Partager

- Sélection de ressources
    - [Enseigner NSI](https://nsi.enseigne.ac-lyon.fr/spip/)
      \dotfill\ 
      site de partage de ressources de l'académie de Lyon
    - [Capytale](https://capytale2.ac-paris.fr/wiki/doku.php?id=start)
      \dotfill\ 
      service de partage d’activités de codage en ligne de l'académie de Paris
    - [Caseine](https://moodle.caseine.org)
      \dotfill\ 
      plateforme d'apprentissage en génie industriel, informatique et mathématiques
    - [Class'Code](https://pixees.fr/classcode-v2)
      \dotfill\ 
      ressources et parcours de formation 
- Listes de ressources
    - <http://catalogue.education-et-numerique.org/>
    - <https://data.abuledu.org/>
    - <http://chticode.info> (pas de licence précisée)
- Des livres libres 
    - <https://fr.flossmanuals.net/>
- Logiciels et services pour les REL
    - <https://www.abuledu.org/>
- Des ressources autour du réseau 
    - <https://www.ffdn.org/wiki/doku.php?id=transmission>

## Conclusion : des conseils

- Produisez des contenus en utilisant des **formats simples** et **partageables**
    - Markdown
    - Jupyter

- Vérifiez **toujours** les licences attachées aux ressources que vous utilisez
    - Creative Commons
    - Préférez **CC-BY**

- **Publiez** vos ressources
    - (Frama)git
    - Nextcloud

- **Échangez** 
    - Forum
    - Groupes

::: { .center }
**Faites du commun**
:::


## Crédits

- Cette présentation et son code source sont mises à disposition selon les termes de la [Licence Creative Commons Attribution - Utilisation non commerciale - Partage dans les Mêmes Conditions 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr) \ccbyncsaeu.

    \vfill

- La présentation au format PDF est disponible à \url{http://bruno.boulgour.com/talks/2021-12-15-diu-eil-libre}

    \vfill

- Le code source Markdown-Pandoc de la présentation est disponible à <https://gitlab.univ-lille.fr/b3/talks-20211215-diu-eil-libre>

    \vfill

- La dernière modification de ce document a eu lieu le 29 juin 2022 à 15h03

<!-- Local Variables: -->
<!-- time-stamp-active: t -->
<!-- time-stamp-pattern: "-7/eu lieu le %:d %:b %:y à %:Hh%02M$" -->
<!-- End: -->
