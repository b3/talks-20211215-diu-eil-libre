QUOI := libertes partage

# les noms des fichiers à produire
PDFS := $(foreach f,$(QUOI),$(addprefix $(f),-ecran.pdf -papier.pdf))

# les images
IMG-SVG := $(wildcard img/*.svg)
IMG-PDF := $(addsuffix .pdf,$(basename $(IMG-SVG)))

# configuration des transformations
PANDOC := pandoc
PANDOC += -t beamer
PANDOC += --pdf-engine-opt=-shell-escape
PANDOC += --template=etc/pandoc-beamer.tex
PANDOC += --lua-filter=etc/pandoc-latex.lua
PANDOC += --slide-level=2
PANDOC += --wrap=preserve
PANDOC += --standalone
export TEXINPUTS := .//:

# on se débarrasse des tabulations
.RECIPEPREFIX := >

# vue écran
%-ecran.pdf: %.md $(IMG-PDF) $(wildcard etc/*)
> $(PANDOC) -V aspectratio=169 $< -o $@

# vue papier (4 diapos par pages)
%-papier.pdf: %.md $(IMG-PDF) $(wildcard etc/*)
> $(PANDOC) -V aspectratio=141 -V handout=handout $< -o $@
> pdfjam --nup 2x2 --landscape --frame=true --keepinfo --quiet --outfile $@- $@
> mv $@- $@

# svg -> pdf
img/%.pdf: img/%.svg
> rsvg-convert -a -f pdf -o $@ $<

# GNU et BSD n'ont pas les mêmes options pour utiliser les ERE
SED := sed $(shell sed v </dev/null >/dev/null 2>&1 && echo " --posix") -E 

## lister les actions possibles
help:
> @tabs 20 ; $(SED) -ne '/^## /h; /^[^.%#\t][^% ]+:/{ G; s/^(.*):(.*##|.*)(.*)/\1\t\3/; P; h }' $(MAKEFILE_LIST)

## fabriquer les PDFs
build: $(PDFS)

## publier les fichiers sur mon site web
pub: $(PDFS)
> scp *.pdf bruno@curitiba:/srv/www/bruno.boulgour.com/talks/2021-12-15-diu-eil-libre

.PHONY: clean reset check

## supprimer les fichiers inutiles
clean:
> @find . -name "*~" -delete -print
> rm -rf _minted-input

## supprimer tous les fichiers regénérables et inutiles
reset: clean
> -$(RM) $(PDFS) $(IMG-PDF)

## vérifier la présence des outils nécessaires
check:
> @which pandoc
> @kpsewhich beamer.cls
> @which rsvg-convert
> @which pdfjam
