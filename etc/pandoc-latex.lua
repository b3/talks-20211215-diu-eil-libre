pandoc.List = require 'pandoc.List'
function Header (header)
   if header.level > 2 then
      if not header.classes:includes("block", 1) then
         local inlines = pandoc.List:new{}
         inlines:extend {pandoc.RawInline('tex', '\\structure{')}
         inlines:extend(header.content)
         inlines:extend {pandoc.RawInline('tex', '}')}
         return pandoc.Plain(inlines)
      end
   end
end

--- center.lua – filter to center image on their line and div content
---
--- ideas from https://pandoc.org/lua-filters.html#center-images-in-latex-and-html-output
--- TODO : automatize that for images alone on a line (without the need to add a final backslash)

if FORMAT:match 'latex' or FORMAT:match 'beamer' then
  function Image (elem)
    if elem.classes:includes("center", 1) then
      return {
        pandoc.RawInline('latex', '\\centerline{'),
        elem,
        pandoc.RawInline('latex', '}')
      }
    else
      return elem
    end
  end
  function Div (elem)
    if elem.classes:includes("center", 1) then
      return {
        pandoc.RawBlock('latex', '\\begin{center}'),
        elem,
        pandoc.RawBlock('latex', '\\end{center}')
      }
    else
      return elem
    end
  end
end

--- svg-image-to-pdf.lua – filter to replace svg filename by pdf ones

if FORMAT:match 'latex' or FORMAT:match 'beamer' then
  function Image (elem)
     elem.src = elem.src:gsub("%.svg$", ".pdf")
     return elem
  end
end
